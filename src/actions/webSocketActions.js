import * as types from './types'

export const onWebSocketConnected = () => ({
  type: types.SOCKET_CONNECTED
})

export const onWebSocketDisconnected = () => ({
  type: types.SOCKET_DISCONNECTED
})

export const onWebSocketReceivedData = (action, data) => ({
  type: types.SOCKET_RECEIVED_DATA,
  action,
  data,
})

