import * as types from './types'

export const onPriceSpreadLoad = (username) => ({
  type: types.PRICE_SPREAD_LOAD,
  username
})

export const onPriceSpreadAdd = (username, from, to) => ({
  type: types.PRICE_SPREAD_ADD,
  username,
  from,
  to
})

export const onPriceSpreadRemove = (username, name) => ({
  type: types.PRICE_SPREAD_REMOVE,
  username,
  name
})

export const onPriceSpreadPlot = (currency) => ({
  type: types.PRICE_SPREAD_PLOT,
  currency
})

export const onPriceSpreadChangeDateRange = (dateRange) => ({
  type: types.CHART_DATE_RANGE_PRICE_SPREAD,
  dateRange
})