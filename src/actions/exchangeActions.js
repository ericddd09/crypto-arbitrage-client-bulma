import * as types from './types'

export const onLoadConfiguredExchanges = (username) => ({
  type: types.EXCHANGE_LOAD,
  username
})

export const onAddConfiguredExchange = (username, exchange) => ({
  type: types.EXCHANGE_ADD,
  username,
  exchange,
})

export const onRemoveConfiguredExchange = (username, exchange) => ({
  type: types.EXCHANGE_REMOVE,
  username,
  exchange,
})

export const onSortConfiguredExchanges = (currency) => ({
  type: types.EXCHANGE_SORT,
  currency
})

export const onPlotCurrency = (currency) => ({
  type: types.EXCHANGE_PLOT,
  currency
})

export const onExchangeDataChangeDateRange = (dateRange) => ({
  type: types.CHART_DATE_RANGE_EXCHANGE_DATA,
  dateRange
})


