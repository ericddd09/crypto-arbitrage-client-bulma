import * as types from './types'

export const onUserLoadSession = () => ({
  type: types.USER_LOAD_SESSION
})

export const onUserLogin = (username) => ({
  type: types.USER_LOGIN,
  username
})


