import * as actions from './userActions'

describe('test actions', () => {
  it('onUserLogin', () => {
    expect(actions.onUserLogin('guest')).toEqual({
      type: 'USER_LOGIN',
      username: 'guest'
    })
  })
})

