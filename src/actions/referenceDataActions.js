import * as types from './types'

export const onLoadAvailableExchanges = () => ({
    type: types.REFERENCE_DATA_LOAD_EXCHANGES
});

