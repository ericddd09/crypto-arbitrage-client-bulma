import * as types from './types'

export const onCurrencyLoad = (username) => ({
  type: types.CURRENCY_LOAD,
  username,
})

export const onCurrencyChange = (username, currency, position) => ({
  type: types.CURRENCY_CHANGE,
  username,
  currency,
  position,
})


