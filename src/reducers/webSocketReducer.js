import * as types from '../actions/types'

const initialState = {
    status: false
};

const webSocketReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.SOCKET_CONNECTED:
            return Object.assign({}, state, {
                status: true
            });

        case types.SOCKET_DISCONNECTED:
            return Object.assign({}, state, {
                status: false
            });

        default:
            return state;
    }
};

export default webSocketReducer;