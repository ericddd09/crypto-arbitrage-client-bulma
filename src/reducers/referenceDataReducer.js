import * as types from '../actions/types'

const getInitialValue = () => {
  return {
    exchanges: []
  }
}

const referenceDataReducer = (state = getInitialValue(), action) => {

  switch (action.type) {

    case types.REFERENCE_DATA_LOAD_EXCHANGES_SUCCESS:
      return Object.assign({}, state, {
        exchanges: action.payload
      })

    default:
      return state
  }
}

export default referenceDataReducer