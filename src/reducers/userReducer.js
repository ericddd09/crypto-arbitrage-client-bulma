import * as types from '../actions/types'

const getInitialValue = () => {
  return {name: 'guest'}
}

const userReducer = (state = getInitialValue(), action) => {
  switch (action.type) {

    case types.USER_LOAD_SESSION_SUCCESS:
      return Object.assign({}, state, {
        ...action.payload,
      })

    case types.USER_LOGIN_SUCCESS:
      return Object.assign({}, state, {
        ...action.payload,
      })

    default:
      return state
  }
}

export default userReducer