import * as types from '../actions/types'

const getInitialValue = () => {
  return {
    data: [],
    lastUpdate: new Date(),
    plot: 'eth',
  }
}

const priceSpreadReducer = (state = getInitialValue(), action) => {
  switch (action.type) {

    case types.SOCKET_PRICE_SPREAD_DATA:
      return Object.assign({}, state, {
        data: action.payload,
        lastUpdate: new Date()
      })

    case types.PRICE_SPREAD_ADD_SUCCESS:
      return Object.assign({}, state, {
        data: state.data.concat(action.payload),
        addMode: false,
        fromExchange: '',
        toExchange: ''
      })

    case types.PRICE_SPREAD_REMOVE_SUCCESS:
      return Object.assign({}, state, {
        data: state.data.filter(item => item.name !== action.name)
      })

    case types.PRICE_SPREAD_PLOT:
      return Object.assign({}, state, {
        plot: action.currency
      })

    default:
      return state
  }
}

export default priceSpreadReducer