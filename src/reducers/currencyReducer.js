import * as types from '../actions/types'

const getInitialValue = () => {
  return ['btc', 'eth', 'bch', 'ltc']
}

const currencyReducer = (state = getInitialValue(), action) => {
  switch (action.type) {

    case types.CURRENCY_LOAD_SUCCESS:
      return action.currencies

    case types.CURRENCY_CHANGE_SUCCESS:
      return action.currencies

    default:
      return state
  }
}

export default currencyReducer