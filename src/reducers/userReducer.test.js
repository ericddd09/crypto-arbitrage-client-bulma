import * as types from '../actions/types'
import userReducer from './userReducer'

describe('UserReducer reducer', () => {

  it('should return the initial state', () => {
    expect(userReducer(undefined, {})).toEqual(
      {
        name: 'guest'
      }
    )
  })

  it('should handle USER_LOAD_SESSION_SUCCESS', () => {
    expect(
      userReducer(undefined, {
        type: types.USER_LOAD_SESSION_SUCCESS,
        payload: {name: 'eric'}
      })
    ).toEqual(
      {
        name: 'eric'
      }
    )
  })
})