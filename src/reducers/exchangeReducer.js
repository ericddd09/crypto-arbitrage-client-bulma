import * as types from '../actions/types'
import {getCommonCurrencies} from '../util/AppUtil'
import _ from 'underscore'

const getInitialValue = () => {
  return {
    data: [],
    currencies: ['btc', 'eth', 'bch', 'ltc'],
    plot: 'eth',
    sort: {orderBy: 'eth', order: 'desc'},
  }
}

const exchangeReducer = (state = getInitialValue(), action) => {

  switch (action.type) {

    case types.EXCHANGE_ADD_SUCCESS:
      return Object.assign({}, state, {
        data: state.data.concat(action.payload),
        currencies: getCommonCurrencies(state.data.concat(action.payload))
      })

    case types.EXCHANGE_REMOVE_SUCCESS:
      return Object.assign({}, state, {
        data: state.data.filter(item => item.name !== action.payload.name),
        currencies: getCommonCurrencies(state.data.filter(item => item.name !== action.payload.name))
      })

    case types.SOCKET_EXCHANGE_DATA:
      return Object.assign({}, state, {
        data: action.payload
      })

    case types.EXCHANGE_SORT:
      return Object.assign({}, state, {
        sort: {orderBy: action.currency, order: (state.sort.order === 'desc' ? 'asc' : 'desc')}
      })

    case types.EXCHANGE_PLOT:
      return Object.assign({}, state, {
        plot: action.currency
      })

    default:
      return state
  }
}

export const getSortedData = (data, sort) => {
  let list = _.sortBy(data, function (item) {
    if (item.currencies[sort.orderBy]) {
      return item.currencies[sort.orderBy].last
    } else {
      return 0
    }
  })

  return sort.order === 'asc' ? list : list.reverse()
}

export default exchangeReducer