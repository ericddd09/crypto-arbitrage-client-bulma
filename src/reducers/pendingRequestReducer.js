import * as types from '../actions/types'

const getInitialValue = () => {
  return {
    count: 0
  }
}

const createPendingRequestReducer = (type = '') => {

  return (state = getInitialValue(), action = {}) => {

    if (action.requestType === type && action.startProgress) {
      return Object.assign({}, state, {
        count: state.count + 1
      })
    } else if (action.requestType === type && action.endProgress) {
      return Object.assign({}, state, {
        count: state.count > 0 ? state.count - 1 : 0
      })
    } else {
      if (action.requestType === type && action.type === types.STOP_SPINNER) {
        return Object.assign({}, state, {
          count: 0
        })
      } else {
        return state
      }
    }
  }
}

export default createPendingRequestReducer

