import { combineReducers } from 'redux';
import user from './userReducer'
import webSocket from './webSocketReducer'
import exchangeData from './exchangeReducer'
import priceSpread from './priceSpreadReducer'
import { createChartReducer } from './chartReducer'
import referenceDataReducer from './referenceDataReducer'
import createPendingRequestReducer from './pendingRequestReducer'
import currencyReducer from './currencyReducer'


export default combineReducers({
    user,
    webSocket,
    pendingExchangeRequest: createPendingRequestReducer("EXCHANGE"),
    pendingPriceSpreadRequest: createPendingRequestReducer("PRICE_SPREAD"),
    exchangeData,
    exchangeDataChart: createChartReducer('EXCHANGE_DATA'),
    priceSpread,
    priceSpreadChart: createChartReducer('PRICE_SPREAD'),
    referenceData: referenceDataReducer,
    currencies: currencyReducer,
})
