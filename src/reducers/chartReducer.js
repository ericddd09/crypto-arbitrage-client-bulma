import * as types from '../actions/types'


const getInitialValue = () => {
    return {
        data: {},
        dateRanges: [1, 12, 24, 7 * 24, 30 * 24, 3 * 30 * 24, 6 * 30 * 24, 12 * 30 * 24],
        dateRange: 1,
        lastUpdate : new Date()
    }
};

export const createChartReducer = (suffix = '') => {

    return (state = getInitialValue(), action = {}) => {
        switch (action.type) {

            case types.CHART_DATE_RANGE + '_' + suffix + '_SUCCESS':
                return Object.assign({}, state, {
                    dateRange: action.dateRange,
                    data: {}
                });

            case types.CHART_UPDATE_DATA + '_' + suffix + '_SUCCESS':
                return Object.assign({}, state, {
                    data: action.payload,
                    lastUpdate : new Date()
                });

            case types.CHART_REMOVE_DATA + '_' + suffix:
                delete state.data[action.name];

                return Object.assign({}, state, {
                    data: state.data
                });

            default:
                return state;
        }
    };
};

