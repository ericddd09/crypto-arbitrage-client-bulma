import React, { Fragment } from 'react'
import ReactGA from 'react-ga'
import { Router, Route } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import HomeContainer from './containers/HomeContainer'

ReactGA.initialize('UA-136330646-2', {
  testMode: process.env.NODE_ENV !== 'production'
})
ReactGA.pageview(window.location.pathname + window.location.search)

const App = () => (
  <Router history={createBrowserHistory()}>
    <Fragment>
      <Route path="/" exact component={HomeContainer}/>
    </Fragment>
  </Router>
)

export default App
