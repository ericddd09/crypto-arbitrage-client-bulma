import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import Header from '../components/Header'
import Home from '../components/Home'
import WebSocketApi from '../api/WebSocketIOApi'

class HomeContainer extends Component {

  componentDidMount () {
    const {username, dispatch} = this.props
    const topic = '/topic/market/' + username
    const params = {action: 'marketdata', username, user: username}
    this.socket = new WebSocketApi(topic, dispatch, params)
    this.socket.connect()
  }

  componentWillUnmount () {
    this.socket.disconnect()
  }

  render () {
    return (
      <Fragment>
        <Header {...this.props}/>
        <Home {...this.props}/>
      </Fragment>
    )
  }
}

export default connect(
  state => ({
    username: state.user.name,
    status: state.webSocket.status,
    inProgress: state.pendingExchangeRequest.count > 0 || state.pendingPriceSpreadRequest.count > 0,
  }),
  dispatch => ({
    dispatch,
  }))(HomeContainer)

