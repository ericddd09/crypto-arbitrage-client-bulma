import React, { Component } from 'react'
import { connect } from 'react-redux'
import queryString from 'query-string'
import Chart from '../components/common/Chart'
import { onPriceSpreadChangeDateRange } from '../actions/priceSpreadActions'
import { parseDateRange } from '../util/AppUtil'

class PriceSpreadChartContainer extends Component {

  componentDidMount () {
    const params = queryString.parse(this.props.location.search)
    this.props.onPriceSpreadChangeDateRange(parseDateRange(params.dateRange))
  }

  render () {
    const {onPriceSpreadChangeDateRange} = this.props

    return (
      <section className="">
        <div className="container">
          <div className="box">
            <Chart
              yAxisLabel={'Price Spread (%)'}
              trackerInfoWidth={150}
              onChangeDateRange={onPriceSpreadChangeDateRange}
              showPoints={true}
              {...this.props} />
          </div>
        </div>
      </section>
    )
  }
}

export default connect(
  state => ({
    plot: state.priceSpread.plot,
    data: state.priceSpreadChart.data,
    lastUpdate: state.priceSpreadChart.lastUpdate,
    dateRanges: state.priceSpreadChart.dateRanges,
    dateRange: state.priceSpreadChart.dateRange,
    pendingRequest: state.pendingPriceSpreadRequest.count > 0
  }),
  {onPriceSpreadChangeDateRange: (dateRange) => onPriceSpreadChangeDateRange(dateRange)}
)(PriceSpreadChartContainer)

