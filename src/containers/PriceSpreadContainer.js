import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  onPriceSpreadLoad,
  onPriceSpreadRemove,
  onPriceSpreadPlot,
} from '../actions/priceSpreadActions'
import PriceSpread from '../components/PriceSpread'
import AddPriceSpreadContainer from './AddPriceSpreadContainer'

class PriceSpreadContainer extends Component {

  componentDidMount () {
    const {username} = this.props
    this.props.onPriceSpreadLoad(username)
  }

  render () {
    const properties = {...this.props, addPriceSpread: <AddPriceSpreadContainer/>}
    return (
      <PriceSpread {...properties}/>
    )
  }
}

export default connect(
  state => ({
    username: state.user.name,
    currencies: state.currencies,
    data: state.priceSpread.data,
    plot: state.priceSpread.plot,
    lastUpdate: state.priceSpread.lastUpdate
  }),
  dispatch => ({
    onPriceSpreadLoad: (username) => dispatch(onPriceSpreadLoad(username)),
    onPriceSpreadPlot: (currency) => dispatch(onPriceSpreadPlot(currency)),
    onPriceSpreadRemove: (username, name) => dispatch(onPriceSpreadRemove(username, name)),
  }))(PriceSpreadContainer)

