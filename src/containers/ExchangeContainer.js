import React, { Component } from 'react'
import { connect } from 'react-redux'
import Exchange from '../components/Exchange'
import { getSortedData } from '../reducers/exchangeReducer'
import {
  onLoadConfiguredExchanges,
  onPlotCurrency,
  onRemoveConfiguredExchange,
  onSortConfiguredExchanges
} from '../actions/exchangeActions'
import { onCurrencyLoad, onCurrencyChange } from '../actions/currencyActions'
import AddExchangeContainer from './AddExchangeContainer'

class ExchangeContainer extends Component {

  componentDidMount () {
    const {username} = this.props
    this.props.onLoadConfiguredExchanges(username)
    this.props.onCurrencyLoad(username)
  }

  onChangeCurrency = (position, item) => {
    this.props.onCurrencyChange(this.props.username, item.newValue, position)
  }

  render () {
    const properties = {...this.props, onChangeCurrency: this.onChangeCurrency, addExchange: <AddExchangeContainer/>}
    return (
      <Exchange {...properties}/>
    )
  }
}

export default connect(
  state => ({
    username: state.user.name,
    currencies: state.currencies,
    data: getSortedData(state.exchangeData.data, state.exchangeData.sort),
    availableCurrencies: state.exchangeData.currencies.map(item => ({code: item, label: item})),
    plot: state.exchangeData.plot,
    sort: state.exchangeData.sort,
  }),
  dispatch => ({
    onLoadConfiguredExchanges: (username) => dispatch(onLoadConfiguredExchanges(username)),
    onRemoveConfiguredExchange: (username, exchange) => dispatch(onRemoveConfiguredExchange(username, exchange)),
    onPlotCurrency: (currency) => dispatch(onPlotCurrency(currency)),
    onSortConfiguredExchanges: (currency) => dispatch(onSortConfiguredExchanges(currency)),
    onCurrencyLoad: (username) => dispatch(onCurrencyLoad(username)),
    onCurrencyChange: (username, currency, position) => dispatch(onCurrencyChange(username, currency, position))
  }))(ExchangeContainer)

