import React, { Component } from 'react'
import { connect } from 'react-redux'
import queryString from 'query-string'
import Chart from '../components/common/Chart'
import { onExchangeDataChangeDateRange } from '../actions/exchangeActions'
import { parseDateRange } from '../util/AppUtil'

class ExchangeChartContainer extends Component {

  componentDidMount () {
    const params = queryString.parse(this.props.location.search)
    this.props.onExchangeDataChangeDateRange(parseDateRange(params.dateRange))
  }

  render () {
    const {onExchangeDataChangeDateRange} = this.props

    return (
      <section className="">
        <div className="container">
          <div className="box">
            <Chart
              yAxisLabel={''}
              max={0.05}
              min={-0.05}
              trackerInfoWidth={120}
              onChangeDateRange={onExchangeDataChangeDateRange}
              showPoints={false}
              {...this.props} />
          </div>
        </div>
      </section>
    )
  }
}

export default connect(
  state => ({
    plot: state.exchangeData.plot,
    data: state.exchangeDataChart.data,
    lastUpdate: state.exchangeDataChart.lastUpdate,
    dateRanges: state.exchangeDataChart.dateRanges,
    dateRange: state.exchangeDataChart.dateRange,
    pendingRequest: state.pendingExchangeRequest.count > 0
  }),
  dispatch => ({onExchangeDataChangeDateRange: (dateRange) => dispatch(onExchangeDataChangeDateRange(dateRange))})
)(ExchangeChartContainer)

