import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { onPriceSpreadAdd } from '../actions/priceSpreadActions'
import { onLoadAvailableExchanges } from '../actions/referenceDataActions'
import AddPriceSpread from '../components/AddPriceSpread'

class AddPriceSpreadContainer extends Component {
  state = {
    editMode: false
  }

  componentDidMount () {
    this.props.onLoadAvailableExchanges()
  }

  toggleEditMode = () => {
    this.setState(prevState => (
      {editMode: !prevState.editMode}
    ))
  }

  render () {
    const {editMode} = this.state
    return (
      <Fragment>
        {!editMode && <Fragment>
          <hr className="hr-break"/>
          <div className="level">
            <div className="level-item" style={{justifyContent: 'left'}}>
              <button type="button" onClick={this.toggleEditMode} className="button">Add Exchange Pair</button>
            </div>
          </div>
        </Fragment>}
        {editMode && <AddPriceSpread toggleEditMode={this.toggleEditMode} {...this.props}/>}
      </Fragment>
    )
  }
}

export default connect(
  state => ({
    username: state.user.name,
    data: state.priceSpread.data,
    exchanges: state.referenceData.exchanges,
  }),
  dispatch => (
    {
      onLoadAvailableExchanges: () => dispatch(onLoadAvailableExchanges()),
      onPriceSpreadAdd: (username, from, to) => dispatch(onPriceSpreadAdd(username, from, to)),
    })
)(AddPriceSpreadContainer)