import React from 'react'
import { shallow } from 'enzyme/build'
import configureStore from 'redux-mock-store'
import HomeContainer from './HomeContainer'

describe('homeController', () => {
  const middlewares = []
  const mockStore = configureStore(middlewares)
  const store = mockStore({
    user: {
      username: 'guest'
    },
    webSocket: {
      status: true
    },
    pendingExchangeRequest: {
      count: 0
    },
    pendingPriceSpreadRequest: {
      count: 0
    }
  })

  it('should render correctly', () => {
    const defaultProps = {store}
    const home = shallow(<HomeContainer {...defaultProps}/>)
    expect(home).toMatchSnapshot()
  })
})

