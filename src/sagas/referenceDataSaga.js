import * as types from '../actions/types'

import {
  takeEvery,
  call,
  put
} from 'redux-saga/effects'

import * as api from '../api/RestApi'
import { handleError } from '../util/errorHandler'

export function * watchLoadReferenceData () {
  yield takeEvery(types.REFERENCE_DATA_LOAD_EXCHANGES, loadAvailableExchangesFlow)
}

function * loadAvailableExchangesFlow (action) {

  try {
    const result = yield call(() => api.exchanges().then(res => res.data))

    // blank value
    let data = [{
      code: '',
      label: ''
    }]

    data = data.concat(result)

    yield put({
      type: types.REFERENCE_DATA_LOAD_EXCHANGES_SUCCESS,
      payload: data
    })

  } catch (error) {
    handleError(error)

    yield put({
      type: types.REFERENCE_DATA_LOAD_EXCHANGES_ERROR,
      error
    })
  }
}


