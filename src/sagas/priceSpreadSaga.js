import {
  takeEvery,
  call,
  put,
  select,
  fork
} from 'redux-saga/effects'
import { TimeEvent } from 'pondjs'
import * as api from '../api/RestApi'
import * as types from '../actions/types'
import { createChartData, createPriceSpreadData } from '../util/AppUtil'
import { handleError } from '../util/errorHandler'

export function * priceSpreadLoadFlow (action) {

  try {
    const result = yield call(() => api.getConfiguredCurrencySpreads(action.username).then(res => res.data))
    const state = yield select()

    for (let item of result) {
      const priceSpread = createPriceSpreadData(item.fromExchange.name + ' vs ' + item.toExchange.name, item.fromExchange.currencies)

      yield put({
        type: types.PRICE_SPREAD_ADD_SUCCESS,
        payload: priceSpread
      })

      yield fork(getHistoricalCurrencySpreadData, action.username, item.fromExchange.name, item.toExchange.name, state.currencies, 1)
    }
  } catch (error) {
    handleError(error)

    yield put({
      type: types.PRICE_SPREAD_ADD_ERROR,
      error,
      requestType: 'PRICE_SPREAD',
      endProgress: true
    })
  }
}

export function * priceSpreadAddFlow (action) {

  try {
    const result = yield call(() => api.addCurrencySpreadConfig(action.username, action.from, action.to).then(res => res.data))
    const state = yield select()

    const priceSpread = createPriceSpreadData(result.fromExchange.name + ' vs ' + result.toExchange.name, result.fromExchange.currencies)

    yield put({
      type: types.PRICE_SPREAD_ADD_SUCCESS,
      payload: priceSpread
    })

    yield fork(getHistoricalCurrencySpreadData, action.username, result.fromExchange.name, result.toExchange.name, state.currencies, 1)

  } catch (error) {
    handleError(error)

    yield put({
      type: types.PRICE_SPREAD_ADD_ERROR,
      error,
      requestType: 'PRICE_SPREAD',
      endProgress: true
    })
  }
}

export function * priceSpreadRemoveFlow (action) {

  try {
    let parts = action.name.split(' vs ')
    let from = parts[0]
    let to = parts[1]

    yield call(() => api.removeCurrencySpreadConfig(action.username, from, to).then(res => res.data))

    yield put({
      type: types.PRICE_SPREAD_REMOVE_SUCCESS,
      name: action.name,
    })

  } catch (error) {
    handleError(error)

    yield put({
      type: types.PRICE_SPREAD_REMOVE_ERROR,
      error,
      requestType: 'PRICE_SPREAD',
      endProgress: true
    })
  }
}

export function * priceSpreadChangeDateRangeFlow (action) {

  try {
    const state = yield select()
    const priceSpreads = state.priceSpread.data

    for (let priceSpread of priceSpreads) {

      const parts = priceSpread.name.split(' vs ')
      const from = parts[0]
      const to = parts[1]

      yield fork(getHistoricalCurrencySpreadData, state.user.name, from, to, state.currencies, action.dateRange)
    }

    yield put({
      type: types.CHART_DATE_RANGE_PRICE_SPREAD_SUCCESS,
      dateRange: action.dateRange
    })

  } catch (error) {
    handleError(error)

    yield put({
      type: types.CHART_DATE_RANGE_PRICE_SPREAD_ERROR,
      dateRange: action.dateRange,
      requestType: 'PRICE_SPREAD',
      endProgress: true
    })
  }
}

export function * getHistoricalCurrencySpreadData (username, from, to, currencies, dateRange) {

  try {
    yield put({
      type: types.CHART_UPDATE_DATA_PRICE_SPREAD,
      payload: {},
      requestType: 'PRICE_SPREAD',
      startProgress: true
    })

    const result = yield call(() => api.getHistoricalCurrencySpreadData(username, from, to, currencies, dateRange).then(res => res.data))
    const state = yield select()

    const chartData = {...state.priceSpreadChart.data}
    chartData[from + ' vs ' + to] = createChartData(dateRange, Object.keys(result.currencies))

    Object.keys(result.currencies).forEach(currency => {
      result.currencies[currency].forEach(ticker => {
        const timestamp = ticker.timestamp
        const value = ticker.last

        if (value && value !== 0) {
          chartData[from + ' vs ' + to].currencies[currency].push(new TimeEvent(new Date(timestamp), value))
        }
      })
    })

    yield put({
      type: types.CHART_UPDATE_DATA_PRICE_SPREAD_SUCCESS,
      payload: chartData,
      requestType: 'PRICE_SPREAD',
      endProgress: true
    })

  } catch (error) {
    handleError(error)

    yield put({
      type: types.CHART_UPDATE_DATA_PRICE_SPREAD_ERROR,
      error,
      requestType: 'PRICE_SPREAD',
      endProgress: true
    })
  }
}

export function * watchPriceSpreadLoad () {
  yield takeEvery(types.PRICE_SPREAD_LOAD, priceSpreadLoadFlow)
}

export function * watchPriceSpreadAdd () {
  yield takeEvery(types.PRICE_SPREAD_ADD, priceSpreadAddFlow)
}

export function * watchPriceSpreadRemove () {
  yield takeEvery(types.PRICE_SPREAD_REMOVE, priceSpreadRemoveFlow)
}

export function * watchPriceSpreadChangeDateRange () {
  yield takeEvery(types.CHART_DATE_RANGE_PRICE_SPREAD, priceSpreadChangeDateRangeFlow)
}


