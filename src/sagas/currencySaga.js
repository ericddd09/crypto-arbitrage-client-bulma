import * as types from '../actions/types'
import { handleError } from '../util/errorHandler'
import {
  takeLatest,
  call,
  put, select
} from 'redux-saga/effects'
import * as api from '../api/RestApi'
import { getHistoricalMarketData } from './exchangeSaga'
import { getHistoricalCurrencySpreadData } from './priceSpreadSaga'

function * currencyLoadFlow (action) {

  try {
    const result = yield call(() => api.loadCurrencies(action.username).then(res => res.data))

    yield put({
      type: types.CURRENCY_LOAD_SUCCESS,
      currencies: result.currencies
    })

    // set default plot values just in case the currencies have changed.
    yield put({
      type: types.EXCHANGE_PLOT,
      currency: result.currencies[1]
    })

    yield put({
      type: types.PRICE_SPREAD_PLOT,
      currency: result.currencies[1]
    })

  } catch (error) {
    handleError(error)

    yield put({
      type: types.CURRENCY_LOAD_ERROR,
      error,
      endProgress: true
    })
  }
}

function * currencyChangeFlow (action) {

  try {
    const state = yield select()
    const prevCurrency = state.currencies

    const result = yield call(() => api.changeCurrency(action.username, action.currency, action.position).then(res => res.data))

    yield put({
      type: types.CURRENCY_CHANGE_SUCCESS,
      currencies: result.currencies
    })

    const exchanges = state.exchangeData.data
    for (let exchange of exchanges) {
      yield call(getHistoricalMarketData, state.user.name, exchange.name, result.currencies,
        state.exchangeDataChart.dateRange)
    }

    const priceSpreads = state.priceSpread.data
    for (let priceSpread of priceSpreads) {
      const parts = priceSpread.name.split(' vs ')
      yield call(getHistoricalCurrencySpreadData, action.username, parts[0], parts[1], result.currencies,
        state.priceSpreadChart.dateRange)
    }

    // update plot option if the currently plotted currency has changed
    if (state.exchangeData.plot === prevCurrency[action.position]) {
      yield put({
        type: types.EXCHANGE_PLOT,
        currency: action.currency
      })
    }

    // do the same for price spread
    if (state.priceSpread.plot === prevCurrency[action.position]) {
      yield put({
        type: types.PRICE_SPREAD_PLOT,
        currency: action.currency
      })
    }

  } catch (error) {
    handleError(error)

    yield put({
      type: types.CURRENCY_CHANGE_ERROR,
      error,
      endProgress: true
    })

  }
}

export function * watchCurrencyLoad () {
  yield takeLatest(types.CURRENCY_LOAD, currencyLoadFlow)
}

export function * watchCurrencyChange () {
  yield takeLatest(types.CURRENCY_CHANGE, currencyChangeFlow)
}
