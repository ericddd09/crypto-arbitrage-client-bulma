import {
  takeEvery,
  call,
  put,
  select,
  fork
} from 'redux-saga/effects'
import { TimeEvent } from 'pondjs'
import * as api from '../api/RestApi'
import * as types from '../actions/types'
import { createChartData, createExchangeData } from '../util/AppUtil'
import { handleError } from '../util/errorHandler'

export function * exchangeDataLoadFlow (action) {

  try {
    const result = yield call(() => api.getConfiguredExchanges(action.username).then(res => res.data))
    const state = yield select()

    for (let item of result) {
      const {exchange: {name, currencies}} = item
      const exchangeData = createExchangeData(name, currencies) // currency object

      yield put({
        type: types.EXCHANGE_ADD_SUCCESS,
        payload: exchangeData
      })

      yield fork(getHistoricalMarketData, action.username, name, state.currencies, state.exchangeDataChart.dateRange)
    }
  } catch (error) {
    handleError(error)

    yield put({
      type: types.EXCHANGE_ADD_ERROR,
      error,
      requestType: 'EXCHANGE',
      endProgress: true
    })
  }
}

export function * exchangeDataAddFlow (action) {

  try {
    const result = yield call(() => api.addExchangeConfig(action.username, action.exchange).then(res => res.data))
    const state = yield select()

    const exchange = createExchangeData(result.exchange.name, result.exchange.currencies) // currency object

    yield put({
      type: types.EXCHANGE_ADD_SUCCESS,
      payload: exchange
    })

    yield fork(getHistoricalMarketData, action.username, result.exchange.name, state.currencies, state.exchangeDataChart.dateRange)

  } catch (error) {
    handleError(error)

    yield put({
      type: types.EXCHANGE_ADD_ERROR,
      error,
      requestType: 'EXCHANGE',
      endProgress: true
    })
  }
}

export function * exchangeDataRemoveFlow (action) {

  try {
    yield call(() => api.removeExchangeConfig(action.username, action.exchange.name).then(res => res.data))

    yield put({
      type: types.EXCHANGE_REMOVE_SUCCESS,
      payload: action.exchange
    })

    yield put({
      type: types.CHART_REMOVE_DATA_EXCHANGE_DATA,
      name: action.exchange.name
    })

  } catch (error) {
    handleError(error)

    yield put({
      type: types.EXCHANGE_REMOVE_ERROR,
      error,
      requestType: 'EXCHANGE',
      endProgress: true
    })
  }
}

export function * exchangeDataChangeDateRangeFlow (action) {

  try {

    const state = yield select()
    const exchanges = state.exchangeData.data

    for (let exchange of exchanges) {
      yield fork(getHistoricalMarketData, state.user.name, exchange.name, state.currencies, action.dateRange)
    }

    yield put({
      type: types.CHART_DATE_RANGE_EXCHANGE_DATA_SUCCESS,
      dateRange: action.dateRange
    })

  } catch (error) {
    handleError(error)

    yield put({
      type: types.CHART_DATE_RANGE_EXCHANGE_DATA_ERROR,
      dateRange: action.dateRange,
      requestType: 'EXCHANGE',
      endProgress: true
    })
  }
}

export function * getHistoricalMarketData (username, exchangeName, currencies, dateRange) {

  try {

    yield put({
      type: types.CHART_UPDATE_DATA_EXCHANGE_DATA,
      payload: {},
      requestType: 'EXCHANGE',
      startProgress: true
    })

    const result = yield call(() => api.getHistoricalMarketData(username, exchangeName, currencies, dateRange).then(res => res.data))
    const state = yield select()

    const chartData = {...state.exchangeDataChart.data}
    chartData[exchangeName] = createChartData(dateRange, Object.keys(result.currencies)) // currency name

    Object.keys(result.currencies).forEach(currency => {
      result.currencies[currency].forEach(ticker => {
        const timestamp = ticker.timestamp
        const value = ticker.last

        if (value > 0) {
          chartData[exchangeName].currencies[currency].push(new TimeEvent(new Date(timestamp), value))
        }
      })
    })

    yield put({
      type: types.CHART_UPDATE_DATA_EXCHANGE_DATA_SUCCESS,
      payload: chartData,
      requestType: 'EXCHANGE',
      endProgress: true
    })

  } catch (error) {
    handleError(error)

    yield put({
      type: types.CHART_UPDATE_DATA_EXCHANGE_DATA_ERROR,
      error,
      requestType: 'EXCHANGE',
      endProgress: true
    })
  }
}

export function * watchExchangeDataLoad () {
  yield takeEvery(types.EXCHANGE_LOAD, exchangeDataLoadFlow)
}

export function * watchExchangeDataAdd () {
  yield takeEvery(types.EXCHANGE_ADD, exchangeDataAddFlow)
}

export function * watchExchangeDataRemove () {
  yield takeEvery(types.EXCHANGE_REMOVE, exchangeDataRemoveFlow)
}

export function * watchExchangeDataChangeDateRange () {
  yield takeEvery(types.CHART_DATE_RANGE_EXCHANGE_DATA, exchangeDataChangeDateRangeFlow)
}


