import { watchLoadReferenceData } from './referenceDataSaga'
import { watchLoadUserSession, watchUserLogin } from './userSaga'
import {
  watchPriceSpreadLoad, watchPriceSpreadRemove, watchPriceSpreadAdd,
  watchPriceSpreadChangeDateRange
} from './priceSpreadSaga'
import {
  watchExchangeDataAdd,
  watchExchangeDataChangeDateRange,
  watchExchangeDataLoad,
  watchExchangeDataRemove
} from './exchangeSaga'

import { watchOnWebSocketReceivedDataFlow } from './webSocketSaga'
import { watchCurrencyLoad, watchCurrencyChange } from './currencySaga'

export default function * rootSaga () {
  yield [watchLoadUserSession(),
    watchUserLogin(),
    watchCurrencyLoad(),
    watchCurrencyChange(),
    watchPriceSpreadLoad(),
    watchPriceSpreadAdd(),
    watchPriceSpreadRemove(),
    watchPriceSpreadChangeDateRange(),
    watchExchangeDataLoad(),
    watchExchangeDataAdd(),
    watchExchangeDataRemove(),
    watchExchangeDataChangeDateRange(),
    watchLoadReferenceData(),
    watchOnWebSocketReceivedDataFlow()]
}
