import {
  takeEvery,
  put,
  select
} from 'redux-saga/effects'
import { TimeEvent } from 'pondjs'
import * as types from '../actions/types'
import { handleError } from '../util/errorHandler'

export function * watchOnWebSocketReceivedDataFlow () {
  yield takeEvery(types.SOCKET_RECEIVED_DATA, onWebSocketReceivedDataFlow)
}

function * onWebSocketReceivedDataFlow ({action, data}) {

  try {

    if (action === 'marketdata') {

      yield put({
        type: types.SOCKET_EXCHANGE_DATA,
        payload: data.exchangeData
      })

      const state = yield select()

      const exchangeChartData = state.exchangeDataChart.data
      for (let exchange of data.exchangeData) {
        const exchangeChart = exchangeChartData[exchange.name]

        if (exchangeChart) {
          const exchangeChartCurrencies = Object.keys(exchangeChart.currencies)
          const exchangeCurrencies = Object.keys(exchange.currencies)

          for (let currency of exchangeCurrencies) {
            if (exchangeChartCurrencies.includes(currency)) {
              addEvent(exchangeChart, exchange, currency)
            } else {
              // console.log('Exchange currency not configured : ' + currency)
            }
          }
        } else {
          console.log('Exchange not configured : ' + exchange.name)
        }
      }

      yield put({
        type: types.CHART_UPDATE_DATA_EXCHANGE_DATA_SUCCESS,
        payload: exchangeChartData
      })

      // Price spread data
      yield put({
        type: types.SOCKET_PRICE_SPREAD_DATA,
        payload: data.priceSpreadData
      })

      const priceSpreadChartData = state.priceSpreadChart.data
      for (let priceSpread of data.priceSpreadData) {
        const priceSpreadChart = priceSpreadChartData[priceSpread.name]

        if (priceSpreadChart) {
          const priceSpreadChartCurrencies = Object.keys(priceSpreadChart.currencies)
          const priceSpreadCurrencies = Object.keys(priceSpread.currencies)

          for (let currency of priceSpreadCurrencies) {
            if (priceSpreadChartCurrencies.includes(currency)) {
              addEvent(priceSpreadChart, priceSpread, currency)
            } else {
              // console.log('Price spread currency not configured : ' + currency)
            }
          }
        } else {
          console.log('Price spread not configured : ' + priceSpread.name)
        }
      }

      yield put({
        type: types.CHART_UPDATE_DATA_PRICE_SPREAD_SUCCESS,
        payload: priceSpreadChartData
      })
    }

  } catch (error) {
    handleError(error)

    yield put({
      type: types.SOCKET_RECEIVED_DATA_ERROR,
      error
    })
  }
}

const addEvent = (chartData, newEvent, currency) => {
  const chartEventQueue = chartData.currencies[currency]
  const lastEvent = chartEventQueue.toArray()[chartEventQueue.count - 1]

  if (lastEvent) {
    const timestamp = newEvent.currencies[currency].timestamp
    const value = newEvent.currencies[currency].last

    if (timestamp && value && (new Date(timestamp).getTime() >= lastEvent.timestamp()) && value !== 0) {
      chartEventQueue.push(new TimeEvent(new Date(timestamp), value))
    }
  }
}


