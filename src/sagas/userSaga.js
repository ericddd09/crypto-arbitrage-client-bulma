import * as types from '../actions/types'

import {
  takeEvery,
  takeLatest,
  call,
  put,
  fork
} from 'redux-saga/effects'

import * as api from '../api/RestApi'
import { handleError } from '../util/errorHandler'

function * loadUserSessionFlow () {
  let payload = sessionStorage.getItem('token') || {name: 'guest'}

  yield put({
    type: types.USER_LOAD_SESSION_SUCCESS,
    payload
  })
}

function * userLoginFlow (action) {

  try {
    const result = yield call(() => api.login(action.username).then(res => res.data))

    yield put({
      type: types.USER_LOGIN_SUCCESS,
      payload: result
    })

    yield fork(saveUserSession, result)

  } catch (error) {
    handleError(error)

    yield put({
      type: types.USER_LOGIN_ERROR,
      error
    })
  }
}

function saveUserSession (payload) {
  sessionStorage.setItem('token', JSON.stringify(payload))
}

export function * watchLoadUserSession () {
  yield takeEvery(types.USER_LOAD_SESSION, loadUserSessionFlow)
}

export function * watchUserLogin () {
  yield takeLatest(types.USER_LOGIN, userLoginFlow)
}

