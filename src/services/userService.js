import api from '../api/RestApi'
import { getSessionItem, handleError } from '../util/BrowserUtil'

export const loadUserSession = () => {
  return getSessionItem('crypto:username' || 'guest')
}

export const createUserAccount = async (username) => {
  try {
    const result = await api.login(username)
      .then(res => res.data)

    return {username: result.name}

  } catch (error) {
    handleError(error)
  }
}

