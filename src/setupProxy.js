const proxy = require('http-proxy-middleware')

module.exports = function (app) {
  app.use(proxy('/ws', {
    target: 'http://localhost:3003/',
    ws: true,
  }))

  app.use(proxy('/api', {
    target: 'http://localhost:3003/',
  }))
}
