import axios from 'axios'

// Login

export const login = username => {
  return axios.post('/api/users', {username: username})
}

// User configuration

export const loadCurrencies = (username) => {
  return axios.get('/api/configure/currencies?username=' + username)
}

export const changeCurrency = (username, currency, position) => {
  return axios.put('/api/configure/currencies', {username, currency, position})
}

export const getConfiguredExchanges = username => {
  return axios.get('/api/configure/exchanges?username=' + username)
}

export const addExchangeConfig = (username, exchange) => {
  return axios.post('/api/configure/exchanges', {username, exchange})
}

export const removeExchangeConfig = (username, exchange) => {
  return axios.delete('/api/configure/exchanges?username=' + username + '&exchange=' + exchange)
}

export const getConfiguredCurrencySpreads = username => {
  return axios.get('/api/configure/currencyspreads?username=' + username)
}

export const addCurrencySpreadConfig = (username, fromExchange, toExchange) => {
  return axios.post('/api/configure/currencyspreads', {username, fromExchange, toExchange})
}

export const removeCurrencySpreadConfig = (username, fromExchange, toExchange) => {
  return axios.delete('/api/configure/currencyspreads?username=' + username + '&fromExchange=' + fromExchange + '&toExchange=' + toExchange)
}

// Market Data

export const getHistoricalMarketData = (username, exchange, currencies, range) => {
  return axios.get('/api/marketdata?username=' + username + '&exchange=' + exchange + '&currencies=' + currencies.join(',') + '&range=' + range)
}

export const getHistoricalCurrencySpreadData = (username, fromExchange, toExchange, currencies, range) => {
  return axios.get('/api/currencyspread?username=' + username + '&fromExchange=' + fromExchange + '&toExchange=' + toExchange + '&currencies=' + currencies.join(',') + '&range=' + range)
}

// Exchange Rate

export const exchangeRate = (from, to) => {
  return axios.get('/api/exchangeRate?from=' + from + '&to=' + to)
}

//  Reference data

export const exchanges = () => {
  return axios.get('/api/exchanges', {

    transformResponse: axios.defaults.transformResponse.concat(data => {
      data.forEach(item => {
        item.code = item.name
        item.label = item.name
      })
      return data
    })
  })
}

export const getExchangeByName = name => {
  return axios.get('/api/exchange?name=' + name)
}

export const currencyPairs = exchange => {
  return axios.get('/api/currencyPairs?exchange=' + exchange, {
    transformResponse: axios.defaults.transformResponse.concat(data => {
      data.forEach(item => {
        item.code = item.name
        item.label = item.name
      })
      return data
    })
  })
}

