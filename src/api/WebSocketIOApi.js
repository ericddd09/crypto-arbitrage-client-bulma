import socket from 'socket.io-client'

import {
  onWebSocketConnected,
  onWebSocketDisconnected,
  onWebSocketReceivedData
} from '../actions/webSocketActions'

class WebSocketIOApi {

  constructor (topic, dispatch, params) {
    this.topic = topic
    this.dispatch = dispatch
    this.params = params
  }

  connect () {
    this.client = socket('/', {path: '/ws', transports: ['websocket', 'polling'], reconnectionAttempts: 10})

    this.client.on('connect', () => {
      console.info('Connected to the websocket server.')
      this.dispatch(onWebSocketConnected())
    })

    this.client.on('message', (data) => {
      // console.log(data)
      this.dispatch(onWebSocketReceivedData(this.params.action, data))
    })

    this.client.on('disconnect', () => {
      console.log('Received disconnect from the websocket server.')
    })

    this.client.on('connect_error', (error) => {
    })

    this.client.on('reconnecting', (attemptNumber) => {
      console.info(`Trying to connect to the websocket server: ${attemptNumber}`)
    })

    this.client.on('reconnect_error', (error) => {
    })

    this.client.on('reconnect_failed', () => {
      console.info('Failed to connect to the websocket server.')
    })
  }

  disconnect () {
    console.log('Closed connection to the websocket server.')
    this.dispatch(onWebSocketDisconnected())
    this.client.disconnect()
  }
}

export default WebSocketIOApi