import SockJS from 'sockjs-client'
import Stomp from 'webstomp-client'
import {
  onWebSocketConnected,
  onWebSocketDisconnected,
  onWebSocketReceivedData
} from '../actions/webSocketActions'

class WebSocketApi {

  constructor (topic, dispatch, params) {
    this.topic = topic
    this.dispatch = dispatch
    this.params = params

    this.connected = false
    this.retryCount = 0
    this.maxRetryCount = 10
    this.retryInterval = 10000
  }

  connect () {
    this.stompClient = Stomp.over(new SockJS('/ws'), {
      protocols: ['v12.stomp'], debug: false,
    })

    const _this = this
    this.stompClient.connect(_this.params, function (frame) {
      console.log('Connected: ' + frame)

      if (_this.timer) {
        clearInterval(_this.timer)
        _this.retryCount = 0
      }

      _this.dispatch(onWebSocketConnected())

      _this.stompClient.subscribe(_this.topic, function (object) {
        const data = JSON.parse(object.body)
        _this.dispatch(onWebSocketReceivedData(_this.params.action, data))
      })

      _this.connected = true

    }, function (error) {
      // disconnect and send error
      _this.disconnect(true)
    })
  }

  disconnect (retry) {
    const _this = this

    if (this.stompClient && this.connected) {
      //if (this.subscription) {
      //    this.unsubscribe();
      //}
      this.stompClient.disconnect(function () {
        console.log('Disconnected from the server')

        _this.connected = false
        _this.dispatch(onWebSocketDisconnected())

        if (retry) {
          _this.timer = setInterval(_this.onTimer.bind(_this), _this.retryInterval)
        }
      })
    }
  }

  onTimer () {
    if (this.retryCount < this.maxRetryCount) {
      this.retryCount++

      if (!this.connected) {
        console.log('Trying to connect to the server ... [' + this.retryCount + ']')
        this.connect()
      }
    } else {
      console.log('Exceeded maximum retry count [' + this.retryCount + ']')
      if (this.timer) {
        clearInterval(this.timer)
      }
    }
  }
}

export default WebSocketApi