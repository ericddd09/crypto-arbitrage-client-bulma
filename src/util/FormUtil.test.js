import { createForm } from './FormUtil'

describe('FormUtils', () => {
  it('creteForm', () => {
    expect(createForm({test: {}})).toEqual({test: {value: '', touched: false, error: '', validationState: ''}})
  })
})