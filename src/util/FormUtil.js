export const createForm = (form) => {
  const merged = {}
  Object.keys(form).forEach(field => {
    const target = {value: '', touched: false, error: '', validationState: ''}
    merged[field] = Object.assign(target, form[field])
  })
  return merged
}

export const handleChange = ({value}) => {
  const error = validate(value)
  return {
    value: value,
    touched: true,
    error: error,
    validationState: validationState({touched: true, error})
  }
}

export const handleBlur = ({value, error}) => {
  return {
    value,
    touched: true,
    error: error,
    validationState: validationState({touched: true, error})
  }
}

const validate = (value) => {
  let error = ''
  if (!isPresent(value)) {
    error = 'Value is required !'
  }
  return error
}

const isPresent = (value) => {
  return !!value
}

const validationState = ({touched, error}) => {
  return touched ? (error ? 'is-danger' : 'is-success') : ''
}

