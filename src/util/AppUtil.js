import Ring from 'ringjs'

const dateRanges = {
  hr: 1,
  hrs: 1,
  d: 24,
  days: 24,
  w: 7 * 24,
  wk: 7 * 24,
  wks: 7 * 24,
  weeks: 7 * 24,
  m: 30 * 24,
  months: 30 * 24,
  y: 12 * 30 * 24,
  years: 12 * 30 * 24,
}

export const createPriceSpreadData = (key, currencies) => {
  const currencyObj = currencies.reduce((data, item) => {
    data[item.base.toLowerCase()] = {}
    return data
  }, {})

  return {
    name: key,
    currencies: currencyObj
  }
}

export const createExchangeData = (key, currencies) => {
  const currencyObj = currencies.reduce((data, item) => {
    data[item.base.toLowerCase()] = {currency: ''}
    return data
  }, {})

  return {
    name: key,
    currencies: currencyObj
  }
}

export const createChartData = (dateRange, currencies) => {
  const currencyObj = currencies.reduce((data, item) => {
    data[item] = new Ring(getBufferSize(dateRange))
    return data
  }, {})

  return {
    currencies: currencyObj
  }
}

export const getBufferSize = range => {
  if (range === 1) {                        // 20 sec interval
    return (60 / 20) * 60 * range
  } else if (range === 12) {                // 5min interval
    return (60 / 5) * range
  } else if (range === 24) {                // 10min interval
    return (60 / 10) * range
  } else if (range === 7 * 24) {            // 60min interval
    return (60 / 60) * range
  } else if (range === 30 * 24) {           // 6hour interval
    return range / 6
  } else if (range === 3 * 30 * 24) {       // 12hour interval
    return range / 12
  } else if (range === 6 * 30 * 24) {       // 24hour interval
    return range / 24
  } else if (range === 12 * 30 * 24) {      // 48hour interval
    return range / 48
  } else {
    console.log('invalid range')
  }
}

export const formatDateRange = item => {
  if (item <= 24) {
    return item + 'hr'
  } else if (item === 3 * 24) {
    return '3d'
  } else if (item === 7 * 24) {
    return '1w'
  } else if (item === 30 * 24) {
    return '1m'
  } else if (item === 3 * 30 * 24) {
    return '3m'
  } else if (item === 6 * 30 * 24) {
    return '6m'
  } else if (item === 12 * 30 * 24) {
    return '1yr'
  } else {
    return item
  }
}

export const parseDateRange = item => {
  let match = /^(\d{1,2})(hr|hrs|d|days|w|wk|wks|weeks|m|months|y|years)$/g.exec(item)
  if (match) {
    return parseInt(match[1]) * dateRanges[match[2]]
  } else {
    return 1 // default
  }
}

export const defaultColours = () => ({
  '1': 'green',
  '2': 'blue',
  '3': 'black',
  '4': 'red',
  '5': 'orange',
  '6': 'gray'
})

export const defaultAxisStyle = () => ({
  labels: {
    labelColor: 'black', // Default label color
    labelWeight: 100,
    labelSize: 12
  },
  axis: {
    axisColor: 'black',
    axisWidth: 1
  }
})

export const formatXAxis = dateRange => {
  if (dateRange <= 24 * 3) {
    return '%a %H:%M %p'
  } else {
    return '%d %B'
  }
}

export const getCommonCurrencies = (data) => {
  const map = new Map()

  data.forEach(exchange => {
    const currencies = Object.keys(exchange.currencies)
    currencies.forEach(currency => {
      if (!map.has(currency)) {
        map.set(currency, [currency])
      } else {
        map.get(currency).push(exchange.name)
      }
    })
  })

  const common = []
  Array.from(map.keys()).forEach(key => {
    if (map.get(key).length === data.length) {
      common.push(key)
    }
  })

  return common
}