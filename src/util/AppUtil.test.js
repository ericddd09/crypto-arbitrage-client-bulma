import {parseDateRange} from './AppUtil'

describe('AppUtils', () => {
  it('parseDateRange', () => {
      expect(parseDateRange('1hr')).toEqual(1)
      expect(parseDateRange('1wk')).toEqual(7 * 24)
      expect(parseDateRange('1m')).toEqual(30 * 24)
  })
})