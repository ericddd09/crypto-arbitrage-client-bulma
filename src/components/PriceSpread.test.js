import React from 'react'
import { shallow } from 'enzyme'
import PriceSpread from './PriceSpread'

describe('', () => {
  const defaultProps = {
    username: 'guest',
    data: [],
    plot: 'eth',
    onPlotCurrency: jest.fn(),
    sort: 'eth',
    currencies: ['btc', 'eth', 'bch', 'ltc'],
    onPriceSpreadPlot: jest.fn(),
    addPriceSpread: jest.fn(),
    onPriceSpreadRemove: jest.fn(),
  }
  it('should render', () => {
    const navigation = shallow(<PriceSpread {...defaultProps}/>)
    expect(navigation).toMatchSnapshot()
  })
})


