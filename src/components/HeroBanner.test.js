import React from 'react'
import { shallow } from 'enzyme'
import HeroBanner from './HeroBanner'

describe('', () => {
  const defaultProps = {
    title: '',
    subtitle: '',
  }
  it('should render', () => {
    const navigation = shallow(<HeroBanner {...defaultProps}/>)
    expect(navigation).toMatchSnapshot()
  })
})


