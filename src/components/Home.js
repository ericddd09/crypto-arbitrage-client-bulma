import React, { Fragment } from 'react'
import HeroBanner from './HeroBanner'
import ExchangeContainer from '../containers/ExchangeContainer'
import ExchangeChartContainer from '../containers/ExchangeChartContainer'
import PriceSpreadContainer from '../containers/PriceSpreadContainer'
import PriceSpreadChartContainer from '../containers/PriceSpreadChartContainer'

const Home = (props) => {
  return (
    <Fragment>
      <HeroBanner title="Market Data" subtitle="(*) Trade Price"/>
      <ExchangeContainer/>
      <div className="is-mt-3">
        <ExchangeChartContainer {...props} />
      </div>
      <HeroBanner title="Price Spread"
                  subtitle="(*) A/ASK price, B/BID price, A/B price spread"/>
      <PriceSpreadContainer/>
      <div className="is-mt-3">
        <PriceSpreadChartContainer {...props}/>
      </div>
    </Fragment>
  )
}

export default Home