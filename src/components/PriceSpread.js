import React, { Fragment, Component } from 'react'
import numeral from 'numeral'
import DeleteRow from './common/DeleteRow'

const Header = ({name, plot, onPlot}) => {
  return (
    <span>
      <input id={`pricespread-${name}`} className="is-checkradio is-info" type="checkbox" checked={name === plot}
             onChange={() => onPlot(name)}/>
      <label htmlFor={`pricespread-${name}`}>{name.toUpperCase()}</label>
     </span>
  )
}

const Cell = ({name, item}) => {
  return (
    <Fragment>
      <span
        className="is-hidden-mobile">{numeral(item.currencies[name] ? item.currencies[name].ask : 0).format('$0,0.00') + ' / ' + numeral(item.currencies[name] ? item.currencies[name].bid : 0).format('$0,0.00') + ' / '}</span>
      <span
        className={cellStyle(item.currencies[name] ? item.currencies[name].last : 0)}>{numeral(item.currencies[name] ? item.currencies[name].last : 0).format('0.00') + '%'}</span>
    </Fragment>
  )
}

const cellStyle = item => {
  if (item > 0) {
    return 'has-text-success has-text-weight-bold'
  } else if (item === 0 || item === undefined) {
    return ''
  } else {
    return 'has-text-danger has-text-weight-bold'
  }
}

class PriceSpread extends Component {

  render () {
    const {
      username,
      currencies,
      data,
      plot,
      onPriceSpreadPlot,
      addPriceSpread,
      onPriceSpreadRemove,
    } = this.props

    return (
      <section className="">
        <div className="container">
          <div className="box">
            <div className="table-container is-mb-0">
              <table className="table is-fullwidth">
                <thead>
                <tr>
                  <th>Exchange Pair (%)</th>
                  <th>
                    <Header name={currencies[0]} plot={plot} onPlot={onPriceSpreadPlot}/>
                  </th>
                  <th>
                    <Header name={currencies[1]} plot={plot} onPlot={onPriceSpreadPlot}/>
                  </th>
                  <th>
                    <Header name={currencies[2]} plot={plot} onPlot={onPriceSpreadPlot}/>
                  </th>
                  <th>
                    <Header name={currencies[3]} plot={plot} onPlot={onPriceSpreadPlot}/>
                  </th>
                  <th>
                  </th>
                </tr>
                </thead>
                <tbody>
                {data && data.map((item, index) => {
                  return (
                    <tr key={index}>
                      <td>
                        {item.name}
                      </td>
                      <td>
                        <Cell name={currencies[0]} item={item}/>
                      </td>
                      <td>
                        <Cell name={currencies[1]} item={item}/>
                      </td>
                      <td>
                        <Cell name={currencies[2]} item={item}/>
                      </td>
                      <td>
                        <Cell name={currencies[3]} item={item}/>
                      </td>
                      <td className="has-text-right">
                        <DeleteRow onProceed={() => onPriceSpreadRemove(username, item.name)}/>
                      </td>
                    </tr>
                  )
                })}
                </tbody>
              </table>
            </div>
            <div className="is-ml-2 is-mt-3 is-mr-2">
              {addPriceSpread}
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default PriceSpread
