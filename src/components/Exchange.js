import React, { Fragment, Component } from 'react'
import numeral from 'numeral'
import Sort from './common/Sort'
import DeleteRow from './common/DeleteRow'
import Select from './common/Select'

const Header = ({position, currencies, currency, onCurrency, plot, onPlot, sort, onSort}) => {
  return (
    <span>

        <input id={`exchange-${currency}`} className="is-checkradio is-info" type="checkbox" checked={currency === plot}
               onChange={() => onPlot(currency)}/>

      <label style={{paddingLeft: '0.8rem'}} htmlFor={`exchange-${currency}`}/>

      <span className="select is-secondary is-small">
        <Select
          options={currencies}
          value={currency}
          onChange={(item) => onCurrency(position, item)}
        />
      </span>

      <Sort className="is-hidden-mobile" direction={sort.order} onSort={() => onSort(currency)}
            active={currency === plot}/>
     </span>
  )
}

const Cell = ({currency, item}) => {
  return (
    <Fragment>
      <span>{numeral(item.currencies[currency] ? item.currencies[currency].last : 0).format('$0,0.00')}</span>
      <span
        className="is-hidden-mobile">{item.currencies[currency] ? (' / ' + item.currencies[currency].currency) : ''}</span>
    </Fragment>
  )
}

class Exchange extends Component {

  render () {
    const {
      username,
      data,
      currencies,
      availableCurrencies,
      onChangeCurrency,
      plot,
      onPlotCurrency,
      sort,
      onSortConfiguredExchanges,
      onRemoveConfiguredExchange,
      addExchange,
    } = this.props

    return (
      <section className="">
        <div className="container">
          <div className="box">
            <div className="table-container is-mb-0">
              <table className="table is-narrow is-fullwidth">
                <thead>
                <tr>
                  <th>Exchange ($AUD)</th>
                  <th>
                    <Header position={0} currencies={availableCurrencies} currency={currencies[0]}
                            onCurrency={onChangeCurrency}
                            plot={plot} onPlot={onPlotCurrency} sort={sort} onSort={onSortConfiguredExchanges}/>
                  </th>
                  <th>
                    <Header position={1} currencies={availableCurrencies} currency={currencies[1]}
                            onCurrency={onChangeCurrency}
                            plot={plot} onPlot={onPlotCurrency} sort={sort} onSort={onSortConfiguredExchanges}/>
                  </th>
                  <th>
                    <Header position={2} currencies={availableCurrencies} currency={currencies[2]}
                            onCurrency={onChangeCurrency}
                            plot={plot} onPlot={onPlotCurrency} sort={sort} onSort={onSortConfiguredExchanges}/>
                  </th>
                  <th>
                    <Header position={3} currencies={availableCurrencies} currency={currencies[3]}
                            onCurrency={onChangeCurrency}
                            plot={plot} onPlot={onPlotCurrency} sort={sort} onSort={onSortConfiguredExchanges}/>
                  </th>
                  <th>
                  </th>
                </tr>
                </thead>
                <tbody>
                {data.map((item, index) => {
                  return (
                    <tr key={index}>
                      <td>
                        {item.name}
                      </td>
                      <td>
                        <Cell currency={currencies[0]} item={item}/>
                      </td>
                      <td>
                        <Cell currency={currencies[1]} item={item}/>
                      </td>
                      <td>
                        <Cell currency={currencies[2]} item={item}/>
                      </td>
                      <td>
                        <Cell currency={currencies[3]} item={item}/>
                      </td>
                      <td className="has-text-right">
                        <DeleteRow onProceed={() => onRemoveConfiguredExchange(username, item)}/>
                      </td>
                    </tr>
                  )
                })}
                </tbody>
              </table>
            </div>
            <div className="is-ml-2 is-mt-3 is-mr-2">
              {addExchange}
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default Exchange