import React from 'react'
import { shallow } from 'enzyme'
import Exchange from './Exchange'

describe('', () => {
  const defaultProps = {
    username: 'guest',
    data: [],
    plot: 'eth',
    onPlotCurrency: jest.fn(),
    sort: 'eth',
    onSortConfiguredExchanges: jest.fn(),
    onRemoveConfiguredExchange: jest.fn(),
    currencies: ['btc', 'eth', 'bch', 'ltc'],
  }
  it('should render', () => {
    const navigation = shallow(<Exchange {...defaultProps}/>)
    expect(navigation).toMatchSnapshot()
  })
})


