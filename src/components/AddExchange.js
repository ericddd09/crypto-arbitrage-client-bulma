import React, { Component } from 'react'
import Select from './common/Select'
import { createForm, handleBlur, handleChange } from '../util/FormUtil'

class AddExchange extends Component {
  state = createForm({
      exchange: {
        value: ''  // assign initial value
      }
    })

  onChange = event => {
    const {exchange} = this.state
    const newState = handleChange({...exchange, value: event.newValue})
    this.setState({exchange: newState})
  }

  onBlur = () => {
    const {exchange} = this.state
    const newState = handleBlur({...exchange})
    this.setState({exchange: newState})
  }

  canSubmit = () => {
    const {exchange: {validationState}} = this.state
    return validationState === 'is-success'
  }

  onSave = () => {
    const {username} = this.props
    const {exchange: {value}} = this.state

    this.props.onAddConfiguredExchange(username, value)
    this.props.toggleEditMode()
  }

  render () {
    const {exchanges, toggleEditMode} = this.props
    const {exchange: {value, error, validationState}} = this.state

    return (
      <div className="box">
        <div className="field is-horizontal">
          <div className="field-label">
            <label htmlFor="exchange" className="label">Exchange:</label>
          </div>
          <div className="field-body">
            <div className="field">
              <div className="control">
                <div className={`select is-fullwidth ${validationState}`}>
                  <Select
                    id="exchange"
                    options={exchanges}
                    value={value}
                    onChange={this.onChange}
                    onBlur={this.onBlur}
                  />
                </div>
              </div>
              {error && <p className={`help ${validationState}`}>{error}</p>}
            </div>
          </div>
        </div>
        <div className="field is-grouped">
          <div className="control">
            <button className="button is-primary" disabled={!this.canSubmit()} onClick={this.onSave}>
              <i className="fa fa-check fa-fw"/>
              <span>Save</span>
            </button>
          </div>
          <div className="control">
            <button className="button" onClick={() => toggleEditMode()}>
              <i className="fa fa-times fa-fw"/>
              <span>Cancel</span>
            </button>
          </div>
        </div>
      </div>
    )
  }
}

export default AddExchange