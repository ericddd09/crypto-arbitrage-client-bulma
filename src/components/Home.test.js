import React from 'react'
import { shallow } from 'enzyme'
import Home from './Home'

describe('', () => {
  const defaultProps = {
    username: 'guest',
    onChange: jest.fn(),
  }
  it('should render', () => {
    const home = shallow(<Home {...defaultProps}/>)
    expect(home).toMatchSnapshot()
  })
})


