import React from 'react'
import { shallow } from 'enzyme'
import Header from './Header'

describe('', () => {
  const defaultProps = {
    username: 'guest',
    status: true,
    inProgress: false,
    onChange: jest.fn(),
  }
  it('should render', () => {
    const header = shallow(<Header {...defaultProps}/>)
    expect(header).toMatchSnapshot()
  })
})


