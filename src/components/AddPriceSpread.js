import React, { Component } from 'react'
import Select from './common/Select'
import { createForm, handleBlur, handleChange } from '../util/FormUtil'

class AddPriceSpread extends Component {
  state = createForm({from: {}, to: {}})

  onChange = (exchange, event) => {
    const newState = handleChange({...this.state[exchange], value: event.newValue})
    this.setState({[exchange]: newState})
  }

  onBlur = (exchange) => {
    const newState = handleBlur({...this.state[exchange]})
    this.setState({[exchange]: newState})
  }

  canSubmit = () => {
    const {from, to} = this.state
    return from.validationState === 'is-success' &&
      to.validationState === 'is-success'
  }

  onSave = () => {
    const {username} = this.props
    const {from, to} = this.state

    if (from.value !== to.value) {
      this.props.onPriceSpreadAdd(username, from.value, to.value)
      this.props.toggleEditMode()

    } else {
      // inject validation error
      this.setState({to: {...this.state.to, error: 'Values must not be equal !', validationState: 'is-danger'}})
    }
  }

  render () {
    const {exchanges, toggleEditMode} = this.props
    const {from, to} = this.state

    return (
      <div className="box">

        <div className="field is-horizontal">
          <div className="field-label">
            <label htmlFor="from" className="label">From:</label>
          </div>
          <div className="field-body">
            <div className="field">
              <div className="control">
                <div className={`select is-fullwidth ${from.validationState}`}>
                  <Select
                    id="from"
                    options={exchanges}
                    value={from.value}
                    onChange={(e) => this.onChange('from', e)}
                    onBlur={() => this.onBlur('from')}/>
                </div>
              </div>
              {from.error && <p className={`help ${from.validationState}`}>{from.error}</p>}
            </div>
          </div>
        </div>

        <div className="field is-horizontal">
          <div className="field-label">
            <label htmlFor="to" className="label">To:</label>
          </div>
          <div className="field-body">
            <div className="field">
              <div className="control">
                <div className={`select is-fullwidth ${to.validationState}`}>
                  <Select
                    id="to"
                    options={exchanges}
                    value={to.value}
                    onChange={(e) => this.onChange('to', e)}
                    onBlur={() => this.onBlur('to')}
                  />
                </div>
              </div>
              {to.error && <p className={`help ${to.validationState}`}>{to.error}</p>}
            </div>
          </div>
        </div>

        <div className="field is-grouped">
          <div className="control">
            <button className="button is-primary" disabled={!this.canSubmit()} onClick={this.onSave}>
              <i className="fa fa-check fa-fw"/>
              <span>Save</span>
            </button>
          </div>
          <div className="control">
            <button className="button" onClick={() => toggleEditMode()}>
              <i className="fa fa-times fa-fw"/>
              <span>Cancel</span>
            </button>
          </div>
        </div>
      </div>
    )
  }
}

export default AddPriceSpread