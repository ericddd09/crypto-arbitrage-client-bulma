import React from 'react'
import { shallow } from 'enzyme'
import AddExchange from './AddExchange'

describe('', () => {
  const defaultProps = {
    toggleEditMode: jest.fn(),
    onAddConfiguredExchange: jest.fn(),
    onLoadAvailableExchanges: jest.fn(),
  }
  it('should render', () => {
    const addExchange = shallow(<AddExchange {...defaultProps}/>)
    expect(addExchange).toMatchSnapshot()
  })
})


