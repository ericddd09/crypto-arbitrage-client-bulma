import React, { Component } from 'react'

class Header extends Component {
  state = {
    open: false
  }

  onToggle = () => {
    this.setState(prevState => {
      return {
        open: !prevState.open
      }
    })
  }

  render () {
    const {open} = this.state
    const {username, status, inProgress} = this.props
    return (
      <nav className="navbar is-fixed-top is-dark" role="navigation" aria-label="main navigation">
        <div className="container">
          <div className="navbar-brand">
            <a className="navbar-item" style={{fontSize: '22px'}} href="/">
              <h1>Crypto Arbitrage</h1>
            </a>

            {/* eslint-disable-next-line */}
            <a role="button" onClick={this.onToggle} className={`navbar-burger burger ${open ? 'is-active' : ''}`}
               aria-label="menu"
               aria-expanded="false" data-target="navbar">
              <span aria-hidden="true"/>
              <span aria-hidden="true"/>
              <span aria-hidden="true"/>
            </a>
          </div>
          <div id="navbar" className={`navbar-menu ${open ? 'is-active' : ''}`}>
            <div className="navbar-end">
              {inProgress && <div className="navbar-item">
                <div className="field is-grouped">
                  <p className="control">
                    <span className="icon">
                        <i className="fas fa-spinner fa-pulse"/>
                    </span>
                  </p>
                  <p className="control">
                    <span>
                        Loading ...
                    </span>
                  </p>
                </div>
              </div>}

              {!status && <div className="navbar-item">
                <div className="field is-grouped">
                  <p className="control">
                    <span className="icon has-text-danger">
                        <i className="fas fa-ban"/>
                    </span>
                  </p>
                  <p className="control">
                    <span>
                        No real-time updates !
                    </span>
                  </p>
                </div>
              </div>}

              <div className="navbar-item">
                <span className="tag is-primary is-medium">{username}</span>
              </div>
            </div>
          </div>
        </div>
      </nav>
    )
  }
}

export default Header