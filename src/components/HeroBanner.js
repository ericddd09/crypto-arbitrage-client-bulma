import React from 'react'

const HeroBanner = ({title, subtitle}) => {
  return (
    <section className="hero">
      <div className="hero-body hero-body-spacing">
        <div className="container">
          <h1 className="title is-5">
            {title}
          </h1>
          <h2 className="subtitle is-6">
            {subtitle}
          </h2>
        </div>
      </div>
    </section>
  )
}

export default HeroBanner
