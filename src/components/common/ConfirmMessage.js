import React, { Component } from 'react'

class ConfirmMessage extends Component {
  state = {
    open: false,
    params: null,
  }

  toggle = () => {
    this.setState(prevState => {
      return {open: !prevState.open}
    })
  }

  open = (params = {}) => {
    this.setState({params: params})
    this.toggle()
  }

  yes = () => {
    this.toggle()
    if (this.props.onYes) {
      this.props.onYes(this.state.params)
    }
  }

  no = () => {
    this.toggle()
  }

  render () {
    const {heading, body} = this.props

    return (
      <div className={`modal ${this.state.open ? 'is-active' : ''}`}>
        <div className="modal-background"/>
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title"> {heading}</p>
            <button className="delete" aria-label="close" onClick={this.no}/>
          </header>
          {body && <section className="modal-card-body">
            {body}
          </section>}
          <footer className="modal-card-foot">
            <button className="button is-success" onClick={this.yes}>
              <i className="fa fa-check fa-fw"/>
              <span>Yes</span>
            </button>
            <button className="button" onClick={this.no}>
              <i className="fa fa-times fa-fw"/>
              <span>No</span>
            </button>
          </footer>
        </div>
      </div>
    )
  }
}

export default ConfirmMessage