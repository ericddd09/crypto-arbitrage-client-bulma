import React from 'react'

const Select = ({id, className, options, value, onChange, onBlur}) => {
  return (
    <select id={id}
            className={className}
            value={value}
            onBlur={onBlur}
            onChange={e => onChange({
              newValue: e.target.value
            })}>
      {
        options.map((item, index) => {
          return (
            <option key={index} value={item.code} defaultValue={value}>
              {item.label.toUpperCase()}
            </option>
          )
        })}
    </select>
  )
}

export default Select



