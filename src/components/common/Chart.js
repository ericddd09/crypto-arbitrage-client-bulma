import React, { Component } from 'react'
import numeral from 'numeral'
import moment from 'moment'
import {
  TimeSeries,
  TimeRange
} from 'pondjs'
import {
  Charts,
  ChartContainer,
  ChartRow,
  YAxis,
  LineChart,
  ScatterChart,
  Resizable,
  Legend,
  styler,
  Baseline
} from 'react-timeseries-charts'

import Loadable from 'react-loading-overlay'
import { formatDateRange, formatXAxis, defaultColours, defaultAxisStyle } from '../../util/AppUtil'

class Chart extends Component {
  state = {
    tracker: null,
    trackerInfoValues: [],
    trackerInfoHeight: 70
  }

  handleTrackerChanged = (tracker) => {
    const {data, plot} = this.props
    const date = new Date(tracker)
    const newTrackerInfoValues = []

    Object.keys(data).forEach(exchange => {
      
      if (data[exchange].currencies[plot]) {
        const events = data[exchange].currencies[plot].toArray()

        const timeSeries = new TimeSeries({
          name: exchange,
          events: events
        })

        const event = timeSeries.atTime(date)
        if (event) {
          let value
          if (exchange.indexOf('vs') > -1) {
            value = numeral(event.get('value')).format('0.00') + '%'
          } else {
            value = numeral(event.get('value')).format('$0,0.00')
          }
          newTrackerInfoValues.push({label: exchange, value})
        }
      } else {
        // ignore
      }
    })

    this.setState({
      tracker: date,
      trackerInfoValues: newTrackerInfoValues,
      trackerInfoHeight: 15 * newTrackerInfoValues.length + 10
    })
  }

  render () {
    const {
      data, plot, min, max, dateRanges, dateRange, onChangeDateRange,
      trackerInfoWidth, yAxisLabel, pendingRequest, showPoints
    } = this.props

    const axisStyle = defaultAxisStyle()
    const colours = defaultColours()

    // convert TimeEvents to TimeSeries object
    const eventSeries = []
    let chartMin = 0
    let chartMax = 0

    Object.keys(data).forEach(exchange => {

      if (data[exchange].currencies[plot]) {
        const events = data[exchange].currencies[plot].toArray()

        const timeSeries = new TimeSeries({
          name: exchange,
          events: events
        })
        eventSeries.push(timeSeries)

        // min / max Chart
        if (events.length > 0) {
          chartMin = min ? ((1 + min) * timeSeries.min()) : Math.min(chartMin, timeSeries.min())
          chartMax = max ? ((1 + max) * timeSeries.max()) : Math.max(chartMax, timeSeries.max())
        }
      } else {
        // console.log('Chart data not found for currency : ' + plot)
      }
    })

    // Chart TimeRange
    const endTime = dateRange === 1 ? moment().toDate() : moment().toDate()
    const beginTime = new Date(endTime.getTime() - 60 * 60 * 1000 * dateRange)
    const timeRange = new TimeRange(beginTime, endTime)

    return (
      <Loadable
        active={pendingRequest}
        background="rgba(0, 0, 0, 0)"
        color="lightgray"
        spinner
        text='Loading ...'>

        <div className="level">
          <div>
            <div className="level-item" style={{marginBottom: '20px'}}>
              {dateRanges.map(function (item, index) {
                return (
                  <button key={index}
                          onClick={() => onChangeDateRange(item)}
                          className={dateRange === item ? 'button is-info is-outlined has-text-weight-bold' : 'button'}>{formatDateRange(item)}</button>
                )
              })}
            </div>
          </div>
          <div>
            <div className="level-item">
              {this.renderLegend(eventSeries, colours)}
            </div>
          </div>
        </div>

        <Resizable>
          <ChartContainer
            timeRange={timeRange}
            timeAxisStyle={axisStyle}
            trackerPosition={this.state.tracker}
            onTrackerChanged={this.handleTrackerChanged}
            format={formatXAxis(dateRange)}>

            <ChartRow
              height="250"
              trackerInfoValues={this.state.trackerInfoValues}
              trackerInfoHeight={this.state.trackerInfoHeight}
              trackerInfoWidth={trackerInfoWidth}>
              <YAxis
                id="price"
                style={axisStyle}
                label={yAxisLabel}
                labelOffset={0}
                min={chartMin}
                max={chartMax}
                width="60"
                format=",.2f"/>

              {this.renderCharts(eventSeries, colours, showPoints)}

            </ChartRow>
          </ChartContainer>
        </Resizable>
      </Loadable>
    )
  }

  renderLegend (eventSeries, colours) {

    let legends = []
    for (var i = 0; i < eventSeries.length; i++) {
      legends.push({
        key: eventSeries[i].name(),
        color: colours[i + 1],
        width: 5,
        label: eventSeries[i].name().toLowerCase(),
      })
    }

    return (
      <Legend
        type="line"
        align="right"
        style={styler(legends)}
        categories={legends}>
      </Legend>)
  }

  renderCharts (eventSeries, colours, showPoints) {

    const baselineStyleLite = {
      line: {
        stroke: 'steelblue',
        strokeWidth: 1,
        opacity: 0.5
      }
    }

    return (
      <Charts>
        {
          eventSeries.map(function (item, index) {
            const style = {
              value: {
                default: {stroke: colours[index + 1], strokeWidth: 1.5, fill: 'none', opacity: 1.0},
                normal: {stroke: colours[index + 1], strokeWidth: 1.5, fill: 'none', opacity: 1.0},
                highlight: {stroke: colours[index + 1], strokeWidth: 1.5, fill: 'none', opacity: 1.0},
                selected: {stroke: colours[index + 1], strokeWidth: 1.5, fill: 'none', opacity: 1.0}
              }
            }
            return (
              <LineChart
                key={item.name}
                axis="price"
                series={item}
                style={style}
                interpolation="curveBasis"/>
            )
          })
        }
        {
          eventSeries.map(function (item, index) {
            const style = {
              value: {
                default: {stroke: colours[index + 1], strokeWidth: 0.05, fill: colours[index + 1], opacity: 1.0},
                normal: {stroke: colours[index + 1], strokeWidth: 0.05, fill: colours[index + 1], opacity: 1.0},
                highlight: {stroke: colours[index + 1], strokeWidth: 0.05, fill: colours[index + 1], opacity: 1.0},
                selected: {stroke: colours[index + 1], strokeWidth: 0.05, fill: colours[index + 1], opacity: 1.0}
              }
            }
            return (
              showPoints ? <ScatterChart
                key={`scatter_${item.name}`}
                axis="price"
                series={item}
                style={style}/> : <React.Fragment key={`scatter_${item.name}`}/>
            )
          })
        }
        <Baseline axis="price" style={baselineStyleLite} value={0}/>
      </Charts>
    )
  }
}

export default Chart


