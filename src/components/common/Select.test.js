import React from 'react';
import { shallow } from 'enzyme'
import Select from './Select';

describe('Select', () => {
  const defaultProps = {
    id: 'test',
    value: '',
    options: []
  };

  it('should render', () => {
    const wrapper = shallow(<Select {...defaultProps}/>);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render with options', () => {
    const props = {
      id: 'test',
      value: '',
      options: [{code: '1', label: 'option_1'}]
    };
    const wrapper = shallow(<Select {...props}/>);
    expect(wrapper).toMatchSnapshot();
  });

  it('should call onChange', () => {
    const props = {
      id: 'test',
      value: '',
      options: [{code: '1', label: 'option_1'}],
      onChange: jest.fn()
    };
    const wrapper = shallow(<Select {...props}/>);
    const element = wrapper.find('#test');
    expect(element.length).toEqual(1);
    element.simulate('change', {target: {value: 'test'}});
    expect(props.onChange).toBeCalledWith({newValue: "test",});
  });
});