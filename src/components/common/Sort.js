import React, { Fragment } from 'react'

const Sort = ({className, direction, onSort, active}) => {
  return active ? (
    <span className={`icon ${className}`} style={{cursor: 'pointer'}} onClick={onSort}>
      {direction === 'asc' && <i className="fas fa-sort-down"/>}
      {direction === 'desc' && <i className="fas fa-sort-up"/>}
      </span>
  ) : (<Fragment/>)
}

export default Sort