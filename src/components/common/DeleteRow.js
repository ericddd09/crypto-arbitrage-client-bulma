import React, { Component, Fragment } from 'react'

class DeleteRow extends Component {
  state = {
    open: false,
  }

  onToggle = () => {
    this.setState(prevState => {
      return {open: !prevState.open}
    })
  }

  render () {
    const {onProceed} = this.props
    const {open} = this.state

    return (
      <Fragment>
        {!open && <button className="button is-normal" onClick={() => this.onToggle()}>
          <i className="fa fa-times fa-fw"/>
          <span>Remove</span>
        </button>}
        {open && <div className="field is-grouped">
          <div className="control">
            <button className="control button is-primary"
                    onClick={onProceed}>Yes?
            </button>
          </div>
          <div className="control">
            <button className="button is-normal"
                    onClick={() => this.onToggle()}>No
            </button>
          </div>
        </div>}
      </Fragment>
    )
  }
}

export default DeleteRow